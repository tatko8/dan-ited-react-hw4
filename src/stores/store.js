import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "@reduxjs/toolkit";
import favoritesReducer from "./reducerFavPage";
import cartReducer from "./reducerCartPage";
import modalReducer from "./reducerMod";

import getAllProductsReducer from "./reducerBooks";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  modalReducer: modalReducer,
  cartReducer: cartReducer,
  favoritesReducer: favoritesReducer,
  getAllProductsReducer: getAllProductsReducer,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: [thunk],
  devTools: true,
});

export default store;
