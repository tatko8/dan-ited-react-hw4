import Cart from "../../components/Cart/Cart";
import EmptyCartIcon from "./IconEmptyCart/IconEmptyCart";
import { removeProdFromCart } from "../../stores/actions";
import { useSelector, useDispatch } from "react-redux";
import Modal from "../../components/Modals/Modal";
import "./CartPage.scss";

function CartPage() {
  const productsInCart = useSelector((state) => state.cartReducer.allProducts);
  const btnMessage = "Delete from cart";
  let key = 1;
  let totalPrice = productsInCart.reduce(
    (acc, item) => acc + Number(item.price),
    0
  );
  console.log(totalPrice);

  console.log(productsInCart);
  const dispatch = useDispatch();
  const submitToRemoveFromCart = () => {
    dispatch(removeProdFromCart());
  };
  return (
    <>
      {Number(productsInCart.length) > 0 ? (
        <>
          <Modal
            header="Delete this item from cart?"
            textContent="Are you sure you want to delete this item to the card? "
            submitBtnFunc={submitToRemoveFromCart}
          />

          <div className="cart-section">
            <ul className="cart__list">
              {productsInCart.map((cartItem) => (
                <Cart
                  id={cartItem.id}
                  key={key++}
                  name={cartItem.name}
                  price={cartItem.price}
                  image={cartItem.image}
                  vin={cartItem.vin}
                  btnMessage={btnMessage}
                />
              ))}
            </ul>
            <div className="cart__total-price">
              Total price:{" "}
              {new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "UAH",
                currencyDisplay: "narrowSymbol",
              }).format(totalPrice)}
            </div>
          </div>
        </>
      ) : (
        <>
          <EmptyCartIcon />
        </>
      )}
    </>
  );
}

export default CartPage;

