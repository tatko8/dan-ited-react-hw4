import CartIconForHeader from "./HeaderBtn/HeaderBtnCart/Cart";
import FavouriteIconForHeader from "./HeaderBtn/HeaderBtnFav/FavBtn";
import GoBackIcon from "./HeaderBtn/IconBack/IconBack";
import { Link } from "react-router-dom";
import "./Header.scss";

function Header() {
  return (
    <div className="header">
      <div className="header__links">
        <Link to="/" className="header__link">
          <div className="header__link-to-main">
            <GoBackIcon /> Main page
          </div>
        </Link>
        <div className="header__links-group">
          <Link to="/cart" className="header__link">
            <CartIconForHeader />
          </Link>
          <Link to="/favourites" className="header__link">
            <FavouriteIconForHeader />
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Header;
