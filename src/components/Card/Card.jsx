import IconFav from "../IconFav/IconFav";
import { addProductToFavorites } from "../../stores/actions";
import { deleteProductFromFavorites } from "../../stores/actions";
import { tryToAddToCart } from "../../stores/actions";
import { useDispatch, useSelector } from "react-redux";
import Button from "../Buttons/Button";
import PropTypes from "prop-types";
import "./Card.scss";

const Card = ({ id, image, name, price, vin, btnMessage }) => {
  const dispatch = useDispatch();
  const allfavorites = useSelector((state) => state.favoritesReducer.favorites);
  const thisCard = { id, name, price, image, vin };

  const tryToAddProdToCart = () => {
    dispatch(tryToAddToCart(thisCard));
  };

  const addToFavs = (thisCard) => {
    dispatch(addProductToFavorites(thisCard));
    console.log(thisCard);
  };
  const removeFromFavs = (id) => {
    dispatch(deleteProductFromFavorites(id));
  };
  const favoritesChecker = (id) => {
    const boolean = allfavorites.some((product) => product.id === id);
    return boolean;
  };

  return (
    <>
      <div className="card">
        <li className="card__item">
          <div className="card__image">
            <img
              className="card__image"
              src={image}
              alt="card of product"
            ></img>
          </div>
          <p className="card__name">{name} </p>
          <p className="card__vendorCode"> Article: {vin}</p>
          <p className="card__price">
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "UAH",
              currencyDisplay: "narrowSymbol",
            }).format(price)}
          </p>
          <div className="card__action-btns">
            <Button
              backgroundColor="#742232"
              text={btnMessage}
              tryToCart={tryToAddProdToCart}
            />
            <IconFav
              thisCard={thisCard}
              addToFavs={addToFavs}
              removeFromFavs={removeFromFavs}
              favoritesChecker={favoritesChecker}
            />
          </div>
        </li>
      </div>
    </>
  );
};

Card.propTypes = {
  id: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  vin: PropTypes.number.isRequired,
  btnMessage: PropTypes.string.isRequired,
};
export default Card;
