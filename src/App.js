import React from "react";
import { Routes, Route } from "react-router-dom";
import Wrapper from "./components/Wrapper/Wrapper";
import Header from "./pages/Header/Header";
import CartPage from "./pages/CartPage/CartPage";
import FavPage from "./pages/FavPage/FavPage";
import Main from "./pages/Main/Main";
import NoPage from "./pages/NoPage/NoPage";

function App() {
  return (
    <>
      <Wrapper>
        <Header />
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/cart" element={<CartPage />} />
          <Route path="/favourites" element={<FavPage />} />
          <Route path="*" element={<NoPage />} />
        </Routes>
      </Wrapper>
    </>
  );
}

export default App;

