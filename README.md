## Завдання

Підключіти бібліотеку Redux.

Для цього необхідно доповнити проект, створений у попередньому домашньому завданні `homework3`.

### Технічні вимоги:

* Підключити бібліотеки `redux`, `react-redux` и `redux-thunk`.
* Після отримання масиву даних за допомогою AJAX запиту, записати їх у `redux store`.
* При виведенні компонентів на сторінку – дані брати з `redux store`.
* При відкритті будь-яких молальних вікон - інформація про те, чи відкрито зараз модальне вікно, повинна зберігатися в `redux store`.
* Всі action-и повинні бути виконані у вигляді функцій використовуючи функціонал `redux-thunk`.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.
